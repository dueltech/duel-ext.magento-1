The Duel_Emails extension has been designed to work in Magento 1 installations. The extension can be installed from Magento Marketplace, or by following the instructions below. If your store is running Magento 2, then please use https://github.com/duel-tech/duel-gallery instead.

Magento marketplace link for this Magento 1 extension:
https://marketplace.magento.com/duel-duel-emails.html

Once you have installed the extension, all information about how to use the extension can be found in this user guide:
https://marketplace.magento.com/media/catalog/product/duel-Duel_Emails-1-1-0-ce/user_guides.pdf

Step ​1: ​Navigate ​to ​the ​root ​directory ​of ​your ​Magento ​1 ​installation

Step ​2: ​Using ​a ​command ​line ​tool ​such ​as ​git, ​or ​by ​downloading ​the ​zip ​file, ​place ​the ​contents
of https://github.com/duel-tech/Duel_Emails ​into ​the ./modman ​folder. ​For ​example, ​using
the ​git ​command ​line ​tool ​you ​can ​run ​this ​command:
git ​clone https://github.com/duel-tech/Duel_Emails.git ​.modman/Duel_Emails

Step ​4: ​The ​folder “<<magento_root>>/.modman/Duel_Emails” ​should ​now ​have ​been ​created.
You ​can ​now ​run ​the ​following ​command ​to ​extract ​the ​subfolders ​to ​the ​correct ​locations
within ​Magento:
“bin/modman ​deploy ​Duel_Emails ​--copy ​--force”
(The “--copy” ​flag ​is ​necessary ​to ​ensure ​that ​files ​are ​created ​rather ​than ​symlinks, ​as ​symlinks
may ​not ​work ​in ​some ​hosting ​environment. “--force” ​is ​not ​strictly ​necessary ​upon ​first ​installation, ​but ​if ​you ​are ​reinstalling ​the ​extension ​or ​getting ​an ​update ​then ​it ​will ​ensure ​that
any ​old ​files ​are ​overwritten).

Step ​5: ​Disable ​the ​cache ​under ​“System ​> ​Cache ​Management”

Step ​6: ​After ​opening ​“System ​> ​Configuration”, ​the ​option ​“Duel” ​should ​now ​be ​available ​in ​the
sidebar.
