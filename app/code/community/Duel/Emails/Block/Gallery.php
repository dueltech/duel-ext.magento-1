<?php
/**
 * Block class for returning information about the Duel gallery for a given product to the calling template file. Instantiated within
 * a product page.
*/
class Duel_Emails_Block_Gallery extends Mage_Catalog_Block_Product_Abstract
{

  public $product;

  public function __construct()
  {
    $this->product = $this->getProduct();
  }

  public function getGallery()
  {
    // this function is called from a product page, and prepares the gallery object for that product.
    $gallery = array();

    // finds the product.
    $gallery['product'] = Mage::getStoreConfig('duelemails_options/emails/brand_short_id') . '/' . $this->product['sku'];

    $gallery['color'] = $this->product['gallery_color'] 
      ? $this->product['gallery_color'] 
      : ("#" . Mage::getStoreConfig('duelemails_options/galleries/default_color'));

    $gallery['background'] = $this->product['gallery_background'] 
      ? $this->product['gallery_background'] 
      : ("#" . Mage::getStoreConfig('duelemails_options/galleries/default_background'));

    
    $gallery['layoutRules'] = array();
    $defaultRules = array(
      'columns' => Mage::getStoreConfig('duelemails_options/galleries/default_columns'),
      'rows' => Mage::getStoreConfig('duelemails_options/galleries/default_rows')
    );

    $productRows = (int)$this->product['gallery_rows'];
    $productColumns = (int)$this->product['gallery_columns'];

    if ($productColumns > 0) {
      $defaultRules['columns'] = $this->product['gallery_columns'];
    }
    if ($productRows > 0) {
      $defaultRules['rows'] = $this->product['gallery_rows'];
    }

    $cssRules = $this->getWidthRules();

    if ($cssRules And !$productColumns And !$productRows) {
      $gallery['layoutRules'] = $cssRules;
    }
    array_push($gallery['layoutRules'], $defaultRules);
    $gallery['position'] = $this->product['duel_page_position'];
    $gallery['selector'] = $this->product['duel_selector'];

    if ($this->product['duel_is_active'] And Mage::getStoreConfig('duelemails_options/galleries/show_galleries')) {
      $gallery['active'] = true;
    } else {
      $gallery['active'] = false;
    }

    $gallery['default_position'] = Mage::getStoreConfig('duelemails_options/galleries/default_position');
    $gallery['default_selector'] = Mage::getStoreConfig('duelemails_options/galleries/custom_position');

    // returns the gallery object to the product template file.
    return json_encode($gallery);

  }

  protected function getWidthRules() {
    $widthRules = Mage::getModel('duel_emails/widthrule')->getCollection()->setOrder('minimum_width', 'ASC');
    $layoutRules = array();
    if ($widthRules) {
      foreach ($widthRules as $widthrule) {
        $row = array();
        $row['mediaQuery'] = '(min-width: ' . $widthrule->getData('minimum_width') . 'px)';
        $row['columns'] = $widthrule->getData('columns') ? $widthrule->getData('columns') : '3';
        $row['rows'] = $widthrule->getData('rows') ? $widthrule->getData('rows') : '12';
        array_push($layoutRules, $row);
      }
    }
    return $layoutRules;
    
  }
}