<?php 
/** Tis field is found in the store config, and allows the user to add css rules about how the Duel gallery should be displayed based 
 * on the width of the iframe.
 */
class Duel_Emails_Block_Adminhtml_Newwidthrule extends
 Mage_Adminhtml_Block_System_Config_Form_Fieldset{
 protected $_dummyElement;
    protected $_fieldRenderer;
    protected $_values;
 
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        
        $html = $this->_getHeaderHtml($element);
        $widthRules = Mage::getModel('duel_emails/widthrule')->getCollection();
        $removeRuleUrl = $this->getUrl('duel-emails-admin/widthrule/removerule');
        $addRuleUrl = $this->getUrl('duel-emails-admin/widthrule/addrule');
        $updateRuleUrl = $this->getUrl('duel-emails-admin/widthrule/updaterule');

        $html.= '<script>
        			function getAddRuleUrl () {
        				
        				document.getElementById("config_edit_form").action = ' . json_encode($addRuleUrl) . ';
        				document.getElementById("config_edit_form").submit();
        				
        			}
        		</script>';
            
 		$html.= $this->getRuleFieldHtml($element, 'minimum_width');
        $html.= $this->getRuleFieldHtml($element, 'rows', 'select');
        $html.= $this->getRuleFieldHtml($element, 'columns', 'select');
        $html .= $this->getAddButtonHtml($element);
        $html .= $this->_getFooterHtml($element);
 
        return $html;
    }
    
    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
        }
        return $this->_fieldRenderer;
    }

    protected function getRuleFieldHtml($fieldset, $property, $type = 'text')
    {
        $optionModel = 'duel_emails_gallery_config/' . $property;
        $label = $property == 'minimum_width' ? 'Minimum screen width (px)' : ucfirst($property);
 
        $field = $fieldset->addField('add_widthrule_' . $property, $type,
            array(
                'name'          => 'addrule_'.$property.'[value]',
                'label'         => $label,
                'values'        => $type == 'select' ? Mage::getModel($optionModel)->toOptionArray() : '',

            ))->setRenderer($this->_getFieldRenderer());
        return $field->toHtml();
    }
    
    protected function getAddButtonHtml ($fieldset) {
        $field = $fieldset->addField('add_rule', 'button', array(
            'value' => Mage::helper('core')->__('Add CSS width rule'),
            'name'  => 'add_rule',
            'class' => 'form-button',
            'onclick' => 'getAddRuleUrl();',
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }
	
   

    
}