<?php
/**
 * This block class contains the form which allows the user to edit products individually, to enable or disable the Duel gallery 
 * for that product and to customise the appearance of the gallery.
*/
class Duel_Emails_Block_Adminhtml_Gallery_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

  protected function _construct()
  {
    $this->_blockGroup = 'duel_emails_adminhtml';
    $this->_controller = 'gallery';
    $this->_headerText = 'Edit Duel gallery & follow-up email';
  }

  protected function _prepareLayout()
  {
    $this->_removeButton('delete');
    $this->_removeButton('reset');

    return parent::_prepareLayout();
  }

}