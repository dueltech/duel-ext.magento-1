<?php
/** 
 * This field is found in the store config, and allows the user to add css rules about how the Duel gallery should be displayed based 
 * on the width of the iframe.
 */ 
class Duel_Emails_Block_Adminhtml_Widthrules extends
 Mage_Adminhtml_Block_System_Config_Form_Fieldset{
 protected $_dummyElement;
    protected $_fieldRenderer;
    protected $_values;
 
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html = $this->_getHeaderHtml($element);
        $widthRules = Mage::getModel('duel_emails/widthrule')->getCollection()->setOrder('minimum_width', 'ASC');
        $removeRuleUrl = $this->getUrl('duel-emails-admin/widthrule/removerule');
        $updateRuleUrl = $this->getUrl('duel-emails-admin/widthrule/updaterule');

        if ($widthRules) {

            foreach ($widthRules as $widthrule) {
                
                $html.= $this->getRuleNumberHtml($element, $widthrule);
                $html.= $this->getRuleFieldHtml($element, $widthrule, 'minimum_width');
                $html.= $this->getRuleFieldHtml($element, $widthrule, 'rows', 'select');
                $html.= $this->getRuleFieldHtml($element, $widthrule, 'columns', 'select');
                $html.= $this->getUpdateButtonHtml($element, $widthrule);
                $html.= $this->getRemoveButtonHtml($element, $widthrule);
            }
            $html.= '<script>
            			function removeRule (id) {
            				console.log(id);
            				document.getElementById("remove_rule").value = id;
            				document.getElementById("config_edit_form").action = ' . json_encode($removeRuleUrl) . ';
            				document.getElementById("config_edit_form").submit();
            			}

                        function updateRule (id) {
                            console.log(id);
                            document.getElementById("update_rule").value = id;
                            document.getElementById("config_edit_form").action = ' . json_encode($updateRuleUrl) . ';
                            document.getElementById("config_edit_form").submit();
                        }
            		</script>';
            $html.= $this->getHiddenInputHtml($element);

            $html.= $element->addField('remove_rule', 'hidden', array(
                'name'  => 'remove_rule',
            ))->setRenderer($this->_getFieldRenderer())->toHtml();

            $html.= $element->addField('update_rule', 'hidden', array(
                'name'  => 'update_rule',
            ))->setRenderer($this->_getFieldRenderer())->toHtml();

        }

        $html .= $this->_getFooterHtml($element);
 
        return $html;
    }
    
    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
        }
        return $this->_fieldRenderer;
    }
    
    protected function getRuleNumberHtml($fieldset, $widthrule)
    {
        $field = $fieldset->addField('heading' . '_' . $widthrule->getId(), 'label',
            array(
                'name'          => 'widthrule_' .$widthrule->getId().'',
                
                'label'         => '<strong>Rule for minimum screen width: ' . $widthrule->getData('minimum_width') .'px</strong>',
            ))->setRenderer($this->_getFieldRenderer());
        return $field->toHtml();
    }

    protected function getRuleFieldHtml($fieldset, $widthrule, $property, $type = 'text')
    {
        $optionModel = 'duel_emails_gallery_config/' . $property;
        $label = $property == 'minimum_width' ? 'Minimum screen width (px)' : ucfirst($property);
 
        $field = $fieldset->addField($property . '_' . $widthrule->getId(), $type,
            array(
                'name'          => 'widthrule_'.$property.'_'.$widthrule->getId().'[value]',
                'label'         => $label,
                'value'         => $widthrule->getData($property),
                
                'values'        => $type == 'select' ? Mage::getModel($optionModel)->toOptionArray() : '',
            ))->setRenderer($this->_getFieldRenderer());
        return $field->toHtml();
    }

    protected function getRemoveButtonHtml ($fieldset, $widthrule) {
        $field = $fieldset->addField('remove_' . $widthrule->getId(), 'button', array(
            'value' => Mage::helper('core')->__('Remove'),
            
            'class' => 'form-button',
            'onclick' => 'removeRule(' . $widthrule->getId() . ');',
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }
    
    protected function getUpdateButtonHtml ($fieldset, $widthrule) {
        $field = $fieldset->addField('update_' . $widthrule->getId(), 'button', array(
            'value' => Mage::helper('core')->__('Update'),
            
            'class' => 'form-button',
            'onclick' => 'updateRule(' . $widthrule->getId() . ');',
        ));

        return $field->toHtml();
    }
	
    

    
}