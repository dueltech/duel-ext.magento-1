<?php
/**
 * This block class just displays a link to the preview url, so that clients can preview the Duel email in a window as opposed to sending
 * test.
*/
class Duel_Emails_Block_Adminhtml_Duelpreview extends Mage_Adminhtml_Block_Widget_Form_Container
{

  public function __construct()
  {
    parent::__construct();
    
    $this->_removeButton('reset');
    $this->_removeButton('save');
    $this->_removeButton('back');
    $this->_blockGroup = 'duel_emails_adminhtml';
    $this->_controller = 'duelpreview';
    
    $previewUrl = Mage::helper("adminhtml")->getUrl("duel-emails-admin/previewpopup/index");
    
    $this->_headerText = 'Duel follow-up email: Send a test or <a target="_blank" style="color: blue;" href="'
    . $previewUrl . '">preview the email in a new window</a>';
  }

  public function getSendUrl()
  {
    $sendUrl = Mage::helper("adminhtml")->getUrl("duel_emails_admin/duelpreview/index");
    return $sendUrl;
  }

}