<?php
/**
 * Block class which builds the url for the Duel JSON product feed, and displays this url.
*/
class Duel_Emails_Block_Adminhtml_Duelfeed extends Mage_Adminhtml_Block_Widget_Grid_Container
{

  public function __construct()
  {
    parent::__construct();

    $this->_removeButton('add');
    $this->_blockGroup = 'duel_emails_adminhtml';
    $this->_controller = 'duelfeed';
    
    // gets the unique hash for the json feed which was stored upon installing the extension.
    $duelHash = 'feeds/duel/' . Mage::getStoreConfig('duelemails_options/galleries/hash');
    $duelFeedUrl = Mage::getUrl($duelHash, array('_secure'=>true));
    $this->_headerText = 'Duel JSON product feed URL:<br/><a style="color: blue;" href="'
    . $duelFeedUrl . '">' . $duelFeedUrl . '</a>';
    
  }

  public function getCreateUrl()
  {
    return $this->getUrl('duel_emails_admin/duelfeed/edit');
  }

}