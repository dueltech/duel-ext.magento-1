<?php
/**
 * This class containes functions for locating files within the installation's log directory, and creating an array of these files. The html * for the "select" dropdownfor these files is created and then returned to the calling block.
*/
class Duel_Emails_Block_Adminhtml_Logviewer extends Mage_Adminhtml_Block_Widget_Container
{

  public function __construct()
  {
    parent::__construct();

    $this->_removeButton('add');
    $this->_blockGroup = 'duel_emails_adminhtml';
    $this->_controller = 'logviewer';
    $this->_headerText = 'View Logs';
  }

  public function getLogFilesSelect()
  {
      $logPath = Mage::getBaseDir('log');
      $logFiles = array();

      if (file_exists($logPath)) {
          foreach (new DirectoryIterator($logPath) as $fileInfo) {
              if ($fileInfo->isDot()) {
                  continue;
              }

              if (preg_match('/[(.log)(.logs)]$/', $fileInfo->getFilename())) {
                  $logFiles [] = array('file' => $fileInfo->getPathname(), 'filename' => $fileInfo->getFilename());
              }
          }
      }

      if (empty($logFiles)) {
          return $this->__('No log files found');
      }

      $html = '<label for="rl-log-switcher">' . $this->__('Please choose a file:') . '</label><select id="rl-log-switcher" name="rl-log-switcher"><option value=""></option>';

      foreach ($logFiles as $l) {
          $html .= '<option value="' . $this->getTailUrl(array('file' => $l['filename'])) . '">' . $l['filename'] . '</option>';
      }

      $html .= '</select>';

      return $html;
  }

 
public function getTailUrl(array $params = array())
{
    $params ['_secure'] = true;

    return $this->helper('adminhtml')->getUrl('duel-emails-admin/logviewer/tail', $params);
}

}