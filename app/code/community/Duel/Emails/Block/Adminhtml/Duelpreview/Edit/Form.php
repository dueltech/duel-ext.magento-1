<?php
/**
 * This form is part of the page which allows the user to send a preview of the Duel email, using either mock data or data from real orders.
*/
class Duel_Emails_Block_Adminhtml_Duelpreview_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

  protected function _prepareForm()
  {

    $form = new Varien_Data_Form(
        array(
            'id' => 'send_form',
            'action' => $this->getUrl(
                'duel_emails_admin/duelpreview/send',
                array(
                  '_current' => 'true',
                  'continue' => 0
                )
            ),
            'method' => 'post'
          )
    );
    $form->setUseContainer(true);
    $this->setForm($form);

    

    $fieldset = $form->addFieldset(
        'duel_preview',
        array(
          'legend' => 'Send a test of the Duel follow-up email'
        )
    );

    $fieldset->addField(
        'preview_type', 'select', array(
              'label' => $this->__('Preview Type'),
              'name'  => 'preview_type',
              'required' => true,
              'values' => array(
                '0' => 'Send preview using mock order',
                '1' => 'Send preview using real order data (Simulate cron job)'
              ),
              'after_element_html' => '<small>Simulating the cron job will use orders from the "duel_emails_followup" table, but send the email to the address inputted below.</small>'
              
        )
    );
    $fieldset->addField(
        'duel_test_email', 'text', array(
              'label' => $this->__('Email address'),
              'name'  => 'duel_test_email',
              'required' => true,
              'value' => Mage::getStoreConfig('trans_email/ident_general/email'),
              
              'after_element_html' => '<button type="submit">Send</button>'
        )
    );
    
    return $this;
  }

}