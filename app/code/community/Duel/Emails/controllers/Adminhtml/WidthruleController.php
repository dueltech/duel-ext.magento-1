<?php
/**
 * This controller handles the addition and remove of (CSS) width rules for the Duel galleries, including the addition , modification,
 * and removal of rules. Ensures that no more than 4 rules are active at any one time, and that the CSS widths make sense for browsers and
 * devices.
*/
class Duel_Emails_Adminhtml_WidthruleController extends Mage_Adminhtml_Controller_Action
{

  public function removeRuleAction()
  {
    $ruleId = $this->getRequest()->getParam('remove_rule');
    $params = $this->getRequest()->getParams();
    
    if ($ruleId) {
      Mage::getModel('duel_emails/widthrule')->load($ruleId)->delete();
      $this->_getSession()->addSuccess($this->__('CSS rule deleted.'));
      return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    } else { 
      $this->_getSession()->addNotice($this->__('Rule not deleted; could not find the specified rule.'));
      return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    }

  }

  public function addRuleAction()
  {
    
    $currentRules = Mage::getModel('duel_emails/widthrule')->getCollection();
    
    if ($currentRules->getSize() > 3) {
      $this->_getSession()->addNotice($this->__('Max. 4 rules - Please delete at least one existing rule before adding a new one.'));
        return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    }

    $params = $this->getRequest()->getParams();

    if ($params['addrule_minimum_width']['value'] < 100) {
      $this->_getSession()->addNotice($this->__('Rule not added; minimum screen width for a rule to take effect is 100px.'));
      return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    }
    
    $entry = array(
      'minimum_width' => $params['addrule_minimum_width']['value'],
      'rows' => $params['addrule_rows']['value'],
      'columns' => $params['addrule_columns']['value']
    );

    $model = Mage::getModel('duel_emails/widthrule');

    try {
      $model->setData($entry)->save();
    } catch (Exception $e) {
      Mage::log($e->getMessage(), null, "system.log");
    }

    $this->_getSession()->addSuccess($this->__('CSS rule added.'));
    return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');

  }

  public function updateRuleAction()
  {
    $updateRuleId = $this->getRequest()->getParam('update_rule');
    $params = $this->getRequest()->getParams();

    if ($params['widthrule_minimum_width_' . $updateRuleId]['value'] < 100) {
      $this->_getSession()->addNotice($this->__('Rule not updated; minimum width for a rule to effect is 100px'));
      return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    }

    if ($updateRuleId) {

      $entry = array(
        'minimum_width' => $params['widthrule_minimum_width_' . $updateRuleId]['value'],
        'rows' => $params['widthrule_rows_' . $updateRuleId]['value'],
        'columns' => $params['widthrule_columns_' . $updateRuleId]['value'],
      );
      try {
        Mage::getModel('duel_emails/widthrule')->load($updateRuleId)->addData($entry)->save();
      } catch (Exception $e) {
        Mage::log($e->getMessage(), null, "system.log");
      }

      $this->_getSession()->addSuccess($this->__('CSS rule updated.'));
      return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');
    }

    $this->_getSession()->addNotice($this->__('Rule not deleted; could not find the specified rule.'));
    return $this->_redirect('adminhtml/system_config/edit/section/duelemails_options');

  }

  protected function _isAllowed()
  {
    return Mage::getSingleton('admin/session')->isAllowed('system/config');
  }

}