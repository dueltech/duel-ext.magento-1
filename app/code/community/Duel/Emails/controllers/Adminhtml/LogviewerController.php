<?php
/**
 * Controller which creates the block for the "View Duel Logs" page, and gets the data from the log files to supply to the page.
*/
class Duel_Emails_Adminhtml_LogviewerController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        // creates the block and renders the template.
        $logBlock = $this->getLayout()
        ->createBlock('duel_emails_adminhtml/logviewer')->setTemplate('duel/logs.phtml');
      
        $this->loadLayout()
        ->_addContent($logBlock)
        ->renderLayout();
    }

    public function tailAction()
    {
        // reads the desired log file and formats it with line breaks, then returns it as html.
        $r = $this->getRequest();

        if (!$r->getParam('file')) {
            $this->getResponse()
                ->setBody('<html><head><title></title></head><body><pre>' .
                    Mage::helper('duel_emails')->__('Please choose a file.') . '</pre></body></html>');

            return;
        }

        $f = Mage::getBaseDir('log') . DS . $r->getParam('file');

        $numberOfLines = 200;
        $handle = fopen($f, "r");
        $lineCounter = $numberOfLines;
        $pos = - 2;
        $beginning = false;
        $text = array();
        while ($lineCounter > 0) {
            $t = " ";
            while ($t != "\n") {
                if (fseek($handle, $pos, SEEK_END) == - 1) {
                    $beginning = true;
                    break;
                }
                $t = fgetc($handle);
                $pos --;
            }
            $lineCounter --;
            if ($beginning) {
                rewind($handle);
            }
            $text[$numberOfLines - $lineCounter - 1] = fgets($handle);
            if ($beginning) {
                break;
            }
        }
        fclose($handle);

        $dlFile = '<a href="' . Mage::helper('adminhtml')->getUrl('duel-emails-admin/logviewer/downloadFile',
                array('f' => $r->getParam('file'))) . '">' . $this->__('Download file') . '</a>';

        return $this->getResponse()->setBody('<html>
                                                <head>
                                                    <title></title>
                                                    <!--<meta http-equiv="refresh" content="10">-->
                                                </head>
                                                <body>
                                                    <pre>' . $dlFile . "\r\n\n" . strip_tags(implode('', $text)) . '</pre>
                                                </body>
                                            </html>');

    }

    /**
     * Download log file
     */
    public function downloadFileAction()
    {
        // handles a download request for the desired file.
        $fileName = $this->getRequest()->getParam('f');
        if (is_null($fileName)) {
            return;
        }

        $file = Mage::getBaseDir('log') . DS . $fileName;

        $this->_prepareDownloadResponse($fileName, file_get_contents($file), 'text/plain', filesize($file));
    }
}