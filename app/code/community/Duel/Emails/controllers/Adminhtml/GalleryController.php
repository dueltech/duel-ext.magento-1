<?php
/**
 * Controller which creates the block for the "Manage Duel Galleries and Emails" page, and also manages actions from that page.
*/
class Duel_Emails_Adminhtml_GalleryController extends Mage_Adminhtml_Controller_Action
{

  public function indexAction()
  {
    // creates the block for the "Manage Duel Galleries and Emails" page.
    $galleryBlock = $this->getLayout()
      ->createBlock('duel_emails_adminhtml/gallery');

      $this->loadLayout()
        ->_addContent($galleryBlock)
        ->renderLayout();
  }

  public function bulkAction()
  {
    // handles creates the block for the "Manage Duel Galleries and Emails > Bulk edit" page. Makes a registry entry of which products are being bulk edited, so that this can be used when an action is posted from this page.
    $entityIds = $this->getRequest()->getPost('entity_id');

    if (!$entityIds) {
      $this->_getSession()->addError($this->__('No product(s) selected to bulk edit.'));
      return $this->_redirect('duel_emails_admin/gallery/index');
    }

    Mage::register('mass_edit_ids', $entityIds);

    $galleryMassEditBlock = $this->getLayout()
      ->createBlock('duel_emails_adminhtml/gallery_bulk')
      ->setData('entityIds', $entityIds);
    $this->loadLayout()
      ->_addContent($galleryMassEditBlock)
      ->renderLayout();

  }


  public function resetDefaultsAction()
  {
    // handles the "reset to default values" action from the "Manage Duel Galleries and Emails" page. Sets all the values relevant to Duel back to the default ones, for all the products which have been chosen.
    $storeId = Mage::app()->getStore()->getStoreId();
    $edits = array(
      'gallery_color' => Mage::getStoreConfig('duelemails_options/galleries/default_color'),
      'gallery_background' => Mage::getStoreConfig('duelemails_options/galleries/default_background'),
      'gallery_rows' => Mage::getStoreConfig('duelemails_options/galleries/default_rows'),
      'gallery_columns' => Mage::getStoreConfig('duelemails_options/galleries/default_columns'),
      'duel_page_position' => Mage::getStoreConfig('duelemails_options/galleries/default_position'),
      'duel_selector' => Mage::getStoreConfig('duelemails_options/galleries/default_selector'),
      'duel_is_active' => Mage::getStoreConfig('duelemails_options/galleries/show_galleries'),
      'duel_email_enabled' =>  Mage::getStoreConfig('duelemails_options/emails/active')
    );

    $entityIds = $this->getRequest()->getPost('entity_id');

    $action = Mage::getModel("catalog/product_action");
    $action->updateAttributes($entityIds, $edits, $storeId);

    return $this->_redirect('duel_emails_admin/gallery/index');
  }

  public function editAction()
  {
    // handles the actions for editing one or more products from the pages "Manage Duel Galleries and Emails > Bulk edit" and "Edit Duel gallery & follow-up email" (for a single product). Makes the relevant updates to the database depending on what product changes were posted.
    $storeId = Mage::app()->getStore()->getStoreId();
    $product = Mage::getModel('catalog/product');

    $singleProductId = $this->getRequest()->getParam('id', false);
    $bulkEditIds = $this->getRequest()->getPost('gallery_bulk_edit_ids');

    if ($singleProductId) {
      $product->load($singleProductId);

      if (!$product->getId()) {
        $this->_getSession()->addError($this->__('This product no longer exists.'));
        return $this->_redirect('duel_emails_admin/gallery/index');
      }
    }
    
    $galleryData = $this->getRequest()->getPost('galleryData');
    $bulkEditIds = $this->getRequest()->getPost('gallery_bulk_edit_ids');


    if ($bulkEditIds And $galleryData) {
      $bulkEditIds = json_decode($bulkEditIds);
      $galleryData = array_diff($galleryData, array("-1", "Don't update this attribute"));

      $action = Mage::getModel("catalog/product_action");
      $action->updateAttributes($bulkEditIds, $galleryData, $storeId);

      $this->_getSession()->addSuccess($this->__('The products have been saved.'));
      return $this->_redirect('duel_emails_admin/gallery/index');
    }
    

    if (!$bulkEditIds And $galleryData) {
      try {
        $product->addData($galleryData);
        $product->save();

        $this->_getSession()->addSuccess($this->__('The product has been saved.'));

        return $this->_redirect(
            'duel_emails_admin/gallery/index',
            array('id' => $product->getId())
        );
      } catch (Exception $e) {
        Mage::logException($e);
        $this->_getSession()->addError($e->getMessage());
      }
    }

    Mage::register('current_product', $product);

    $galleryEditBlock = $this->getLayout()->createBlock('duel_emails_adminhtml/gallery_edit');

    $this->loadLayout()
      ->_addContent($galleryEditBlock)
      ->renderLayout();
  }

  protected function _isAllowed()
  {
    return Mage::getSingleton('admin/session')->isAllowed('system/config');
  }

}