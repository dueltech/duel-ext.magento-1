<?php
/**
 * Controller which creates handles the request to preview the Duel email in a pop-up window. Creates a mock order with three products in it, loads the information about the "order" into templates, and returns it as html.
*/
class Duel_Emails_Adminhtml_PreviewpopupController extends Mage_Adminhtml_Controller_Action
{

  public function hasAction($action)
  {
      return true;
  }
  
  public function indexAction()
  {
    $store = Mage::app()->getStore()->getFrontendName();
    $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
    $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

    $brandId = Mage::getStoreConfig('duelemails_options/emails/brand_short_id');
    $templateId = Mage::getStoreConfig('duelemails_options/emails/duel_emails_template');

    if (!$templateId or $templateId == 'duelemails_options_emails_duel_emails_template') {
        $emailTemplate = Mage::getModel('core/email_template')
        ->loadDefault('duel_emails_template');
    } else {
        $emailTemplate = Mage::getModel('core/email_template')
        ->load($templateId);
    }   

    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection->getSelect()->limit(3);

    $baseMediaUrl = Mage::getSingleton('catalog/product_media_config')->getBaseMediaUrl();
    $placeholder = $baseMediaUrl . '/placeholder/' . Mage::getStoreConfig("catalog/placeholder/thumbnail_placeholder");
    
    $items = array();
    foreach ($collection as $item) {
        $row = array();
        $id = $item->getData('entity_id');
        $product = Mage::getModel('catalog/product')->load($id);
        $sku;
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
        if ($parentIds) {
            $row['sku'] = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
        } else {
            $row['sku'] = $item->getSku();
        }

        if (!$product->getData('thumbnail') Or $product->getData('thumbnail') == 'no_selection') {
            $row['thumbnail'] = $placeholder;
        } else {
            $row['thumbnail'] = $baseMediaUrl . $product->getData('thumbnail');
        }
        $row['name'] = $product->getName();
        $row['ctaText'] = $product->getData('duel_cta_text') ? $product->getData('duel_cta_text') : 'Upload your photo now!';

        array_push($items, $row);
    }
    
    $emailTemplateVars = array();
    $emailTemplateVars['store'] = Mage::app()->getStore();
    $emailTemplateVars['order_id'] = 100;
    $emailTemplateVars['brand_id'] = Mage::getStoreConfig('duelemails_options/emails/brand_short_id');
    $emailTemplateVars['order_items'] = $items;

    $emailContent =  $emailTemplate->getProcessedTemplate($emailTemplateVars);

    $this->getResponse()
    ->clearHeaders()
    ->setHeader('Content-Type', 'text/html')
    ->setBody($emailContent);
    
  }

  protected function _isAllowed()
  {
    return Mage::getSingleton('admin/session')->isAllowed('system/config');
  }
}