<?php

class Duel_Emails_Adminhtml_DuelpreviewController extends Mage_Adminhtml_Controller_Action
{

  public function hasAction($action)
  {
      return true;
  }

  public function indexAction()
  {
    $duelpreviewBlock = $this->getLayout()
      ->createBlock('duel_emails_adminhtml/duelpreview');
    
    $this->loadLayout()
      ->_addContent($duelpreviewBlock)
      ->renderLayout();
    
  }

  public function sendAction()
  {
    
    $previewType = $this->getRequest()->getPost('preview_type');


    $config = array();
    $config['logging'] = Mage::getStoreConfig('duelemails_options/emails/logging');
    if ($config['logging']) {
      $config['duelFile'] = fopen('./var/log/duel_preview.log', 'a');
      $dateNow = Mage::getModel('core/date')->date('Y-m-d H:i:s');
      fwrite($config['duelFile'], $dateNow . "\n");
    }
    $config['toEmail'] = $this->getRequest()->getPost('duel_test_email');
    $config['store'] = Mage::app()->getStore()->getFrontendName();
    $config['senderName'] = Mage::getStoreConfig('trans_email/ident_general/name');
    $config['senderEmail'] = Mage::getStoreConfig('trans_email/ident_general/email');
    $config['active'] = Mage::getStoreConfig('duelemails_options/emails/active');
    $config['brandId'] = Mage::getStoreConfig('duelemails_options/emails/brand_short_id');
    if (!$config['brandId']) {
      $this->_getSession()->addWarning($this->__('No brand Short ID found in System > Configuration > Duel Configuration so follow-up emails will not be sent.'));
      if ($config['logging']) {
        fwrite($config['$duelFile'], "No brand Short ID was found in System > Configuration > Duel > Duel Configuration, so follow-up emails will not be sent." . "\n");
      }
    }
    if (!$config['active']) {
      $this->_getSession()->addWarning($this->__('Follow-up emails are disabled in System > Configuration > Duel Configuration.'));
      if ($config['logging']) {
        fwrite($config['$duelFile'], "Duel emails are not enabled in System > Configuration > Duel > Duel Configuration, so follow-up emails will not be sent." . "\n");
      }
    }
    $templateId = Mage::getStoreConfig('duelemails_options/emails/duel_emails_template');

    if (!$templateId or $templateId == 'duelemails_options_emails_duel_emails_template') {
        $config['emailTemplate'] = Mage::getModel('core/email_template')
        ->loadDefault('duel_emails_template');
    } else {
        $config['emailTemplate'] = Mage::getModel('core/email_template')
        ->load($templateId);
    }   
    $config['baseMediaUrl'] = Mage::getSingleton('catalog/product_media_config')->getBaseMediaUrl();
    $config['placeholder'] = $config['baseMediaUrl'] . '/placeholder/' . Mage::getStoreConfig("catalog/placeholder/thumbnail_placeholder");
    
    if (!$config['toEmail']) {
      $this->_getSession()->addError($this->__('No email address provided.'));
    } elseif ($previewType == 1) {
      $this->prepareEmail($config);
      
    } else {
      $this->sendMockEmail($config);
      $this->_getSession()->addSuccess($this->__('Preview email sent.'));
    }
    return $this->_redirect('duel_emails_admin/duelpreview/index');
  }

  protected function sendMockEmail($config)
  {
    if ($config['logging']) {
      fwrite($config['duelFile'], "Send test email using mock order." . "\n");
    }
    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection->getSelect()->limit(10);

    $items = array();
    foreach ($collection as $item) {
        $row = array();
        $id = $item->getData('entity_id');
        $product = Mage::getModel('catalog/product')->load($id);
        $sku;
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
        if ($parentIds) {
            $row['sku'] = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
        } else {
            $row['sku'] = $item->getSku();
        }

        if (!$product->getData('thumbnail') Or $product->getData('thumbnail') == 'no_selection') {
            $row['thumbnail'] = $config['placeholder'];
        } else {
            $row['thumbnail'] = $config['baseMediaUrl'] . $product->getData('thumbnail');
        }

        $row['name'] = $product->getName();
        $row['ctaText'] = $product->getData('duel_cta_text') ? $product->getData('duel_cta_text') : 'Upload your photo now!';

        array_push($items, $row);
        
    }
    
    $emailTemplateVars = array();
    $emailTemplateVars['store'] = Mage::app()->getStore();
    $emailTemplateVars['order_id'] = 100;
    $emailTemplateVars['brand_id'] = $config['brandId'];
    $emailTemplateVars['order_items'] = $items;

    $emailContent =  $config['emailTemplate']->getProcessedTemplate($emailTemplateVars);

    $config['emailTemplate']->setSenderEmail($config['senderEmail']);
    $config['emailTemplate']->setSenderName($config['senderName']);
    try {
      $config['emailTemplate']->send($config['toEmail'], 'Admin', $emailTemplateVars);
      if ($config['logging']) {
        fwrite($config['duelFile'], "No errors reported; test email sent." . "\n");
      }
    } catch (Exception $e) {
      if ($config['logging']) {
        fwrite($config['duelFile'], "Error sending test email; please check mail provider logs." . "\n");
      }
    }
    
  }

  protected function prepareEmail($config)
  {
    
    if ($config['logging']) {
      fwrite($config['duelFile'], "Attempt to send test email using 5 most recent entries from duel_emails_followup table." . "\n");
    }

    $model = Mage::getModel('duel_emails/followup');
    $followupOrders = $model->getCollection()->setPageSize(5)->setOrder('followup_id', 'DESC');

    if ($followupOrders->getSize() == 0) {
      if ($config['logging']) {
        fwrite($config['duelFile'], "No pending follow-up emails were found. Attempt to simulate Duel cron job using 5 most recent customer orders." . "\n");
      }
      $orderCollection = Mage::getModel('sales/order')->getCollection()->setPageSize(5)->addAttributeToSort('entity_id', 'DESC');
      
      if (!$orderCollection->getSize()) {
        fwrite($config['duelFile'], "No pending follow-up emails or customer orders were found. Test email not sent." . "\n");
        $this->_getSession()->addNotice($this->__("No pending follow-up emails or customer orders were found. Test email not sent."));
      } else {
        fwrite($config['duelFile'], $orderCollection->getSize() . " customer orders were found." . "\n");
        foreach ($orderCollection as $order) {
          $orderId = $order->entity_id;
          $this->sendEmail($orderId, $config);
        }
      }
    } elseif ($config['logging']) {
        fwrite($config['duelFile'], $followupOrders->getSize() . " pending follow-up emails were found." . "\n");
        foreach ($followupOrders as $followupOrder) {
          $orderId = $followupOrder->order_id;
          $this->sendEmail($orderId, $config);
        }
    }

    // Iterate over orders
    
  }

  protected function sendEmail($orderId, $config) {
    
    
    $order = Mage::getModel('sales/order')->load($orderId);
    $items = $this->generateItemHtml($order, $config);

    if (!$items And $config['logging']) {
      fwrite($config['duelFile'], "Order ID: " . $orderId . " has no items." . "\n");
    }
     
    $config['emailTemplate']->setSenderEmail($config['senderEmail']);
    $config['emailTemplate']->setSenderName($config['senderName']);
    $toName = $order->getCustomerFirstname()." ".$order->getCustomerLastname();

    $variables = array(
      'items' => $items,
      'customer_name' => $toName,
      'order' => $order,
      'order_id' => $orderId,
      'shop' => $config['store'],
      'brand_id' => $config['brandId'],
      'order_items' => $items
    );

    try {
      $config['emailTemplate']->send($config['toEmail'], $toName, $variables);
      if ($config['logging']) {
        fwrite($config['duelFile'], "Duel test email sent for order with ID: " . $orderId . "\n");
      }
      $this->_getSession()->addSuccess($this->__('Test email sent.'));
    }
    catch (Exception $e) {
      if ($config['logging']) {
        fwrite($config['duelFile'], "Duel test email reported send error for order with ID: " . $orderId . "\n");
      }
    }
    
  }
  
  protected function getfollowup_orders($order) 
  {
    $items = $order->getAllVisibleItems();
    $followupOrders = array();

    foreach ($items as $item) {
      // Get SKU / parent SKU if it's a configurable product
      $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
      $sku;
      if ($parentIds) {
        $sku = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
      } else {
        $sku = $item->getSku();
      }      

      // Check if item has been refunded (full quantity)
      $refunded = $item->getQtyShipped() === $item->getQtyRefunded();

      if (!$refunded) {
        array_push($followupOrders, $item);
      }
    }

    return $followupOrders;
  }

  protected function generateItemHtml($order, $config) 
  {
    $duelItems = $this->getfollowup_orders($order);

    if (count($duelItems) === 0) {
      return null;
    }
    
    $items = array();
    
    foreach ($duelItems as $item) {
        $row = array();
        $id = $item->getData('entity_id');
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        $sku;
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
        if ($parentIds) {
            $row['sku'] = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
        } else {
            $row['sku'] = $item->getSku();
        }

        if (!$product->getData('thumbnail') Or $product->getData('thumbnail') == 'no_selection') {
            $row['thumbnail'] = $config['placeholder'];
        } else {
            $row['thumbnail'] = $config['baseMediaUrl'] . $product->getData('thumbnail');
        }
        $row['name'] = $product->getData('name');
        
        $row['ctaText'] = $product->getData('duel_cta_text') ? $product->getData('duel_cta_text') : 'Upload your photo now!';
        
        array_push($items, $row);
    }

    return $items;
  }

  protected function _isAllowed()
  {
    return Mage::getSingleton('admin/session')->isAllowed('system/config');
  }

}