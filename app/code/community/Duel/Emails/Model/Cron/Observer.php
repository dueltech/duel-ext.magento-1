<?php
/**
 * Cron Observer which checks if there are any orders in the duel_emails_followup table; if yes, then check if they have any products
 * for which Duel emails are enabled. Then load the order information into the relevant templates and send the email.
*/
class Duel_Emails_Model_Cron_Observer
{

  const DEFAULT_INTERVAL = 5;

  public function sendMail() 
  {
    
    $logging = Mage::getStoreConfig('duelemails_options/emails/logging');
    $duelFile = fopen('./var/log/duel_cron.log', 'a');
    
    if ($logging) {
      fwrite($duelFile, Mage::getModel('core/date')->date('Y-m-d H:i:s') . "\n");
      fwrite($duelFile, "Duel cron job." . "\n");
    }
    $store = Mage::app()->getStore()->getFrontendName();
    $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
    $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
    $interval = Mage::getStoreConfig('duelemails_options/emails/email_delay');
    $active = Mage::getStoreConfig('duelemails_options/emails/active');
    $brandId = Mage::getStoreConfig('duelemails_options/emails/brand_short_id');
    $templateId = Mage::getStoreConfig('duelemails_options/emails/duel_emails_template');
    
    $baseMediaUrl = Mage::getSingleton('catalog/product_media_config')->getBaseMediaUrl();
    $placeholder = $baseMediaUrl . '/placeholder/' . Mage::getStoreConfig("catalog/placeholder/thumbnail_placeholder");

    if (!$active) {
      if ($logging) {
        fwrite($duelFile, "Duel emails are not enabled in System > Configuration > Duel > Duel Configuration, so follow-up emails will not be sent." . "\n");
      }
      return;
    }

    if (!$brandId) {
      if ($logging) {
        fwrite($duelFile, "No brand Short ID was found in System > Configuration > Duel > Duel Configuration, so follow-up emails will not be sent." . "\n");
      }
      return;
    }

    $dateNow = Mage::getModel('core/date')->timestamp(time());

    if (!$templateId or $templateId == 'duelemails_options_emails_duel_emails_template') {
        $template = Mage::getModel('core/email_template')
        ->loadDefault('duel_emails_template');
    } else {
        $template = Mage::getModel('core/email_template')
        ->load($templateId);
    }   

    if ((int)$interval !== 0 or $interval === '0') {
      $interval = (int)$interval;
    } else {
      $interval = self::DEFAULT_INTERVAL;
    }

    $model = Mage::getModel('duel_emails/followup');
    $followupOrders = $model->getCollection();
    if ($followupOrders->getSize() == 0) {
      if ($logging) {
        fwrite($duelFile, "No pending follow-up emails were found." . "\n");
      }
    } elseif ($logging) {
        fwrite($duelFile, $followupOrders->getSize() . " pending follow-up email(s) were found." . "\n");
    }
    
    // Iterate over orders
    foreach ($followupOrders as $followupOrder) {
      $deadline = strtotime($followupOrder->completed) + (86400 * $interval);
      $deadlinePassed = $dateNow - (int)$deadline;

      if ($deadlinePassed > 1296000) {
        $model->load($followupOrder->followup_id);
        $model->delete();
      } elseif ($deadlinePassed > 0) {
        $orderId = $followupOrder->order_id;
        
        $order = Mage::getModel('sales/order')->load($orderId);

        $items = $this->generateItemHtml($order, $brandId, $baseMediaUrl, $placeholder);
    
        if ($items) {
         
          $template->setSenderEmail($senderEmail);
          $template->setSenderName($senderName);
          $toName = $order->getCustomerFirstname()." ".$order->getCustomerLastname();

          $variables = array(
            'items' => $items,
            'customer_name' => $toName,
            'order' => $order,
            'order_id' => $orderId,
            'shop' => $store,
            'brand_id' => Mage::getStoreConfig('duelemails_options/emails/brand_short_id'),
            'order_items' => $items
          );

          try {
            $toEmail = $order->getCustomerEmail();
            $template->send($toEmail, $toName, $variables);
            $model->load($followupOrder->followup_id);
            $model->delete();
          }
          catch (Exception $e) {
            $model->load($followupOrder->followup_id);
            $model->delete();
            if ($logging) {
              fwrite($duelFile, "Duel cron job send error or model delete error" . "\n");
            }
          }
        } else {
          $model->load($followupOrder->followup_id);
          $model->delete();
        }
      }
    }

  }
  
  protected function getfollowup_orders($order) 
  {
    $items = $order->getAllVisibleItems();
    $followupOrders = array();

    foreach ($items as $item) {
      // Get SKU / parent SKU if it's a configurable product
      $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
      $sku;
      if ($parentIds) {
        $sku = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
      } else {
        $sku = $item->getSku();
      }      

      // Check if item has been refunded (full quantity)
      $refunded = $item->getQtyShipped() === $item->getQtyRefunded();

      if (!$refunded) {
        array_push($followupOrders, $item);
      }
    }

    return $followupOrders;
  }

  protected function generateItemHtml($order, $brandId, $baseMediaUrl, $placeholder) 
  {
    $duelItems = $this->getfollowup_orders($order);

    if (count($duelItems) === 0) {
      return null;
    }
    
    $items = array();
    
    foreach ($duelItems as $item) {
        $row = array();
        $id = $item->getData('entity_id');
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        $sku;
        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
        if ($parentIds) {
            $row['sku'] = Mage::getModel('catalog/product')->load($parentIds[0])->getSku();
        } else {
            $row['sku'] = $item->getSku();
        }

        if (!$product->getData('thumbnail') Or $product->getData('thumbnail') == 'no_selection') {
            $row['thumbnail'] = $placeholder;
        } else {
            $row['thumbnail'] = $baseMediaUrl . $product->getData('thumbnail');
        }
        $row['name'] = $product->getData('name');
        
        $row['ctaText'] = $product->getData('duel_cta_text') ? $product->getData('duel_cta_text') : 'Upload your photo now!';
        
        if ($product->getData('duel_email_enabled')) {
          array_push($items, $row);
        }
        
    }
    

    return $items;
  }


}