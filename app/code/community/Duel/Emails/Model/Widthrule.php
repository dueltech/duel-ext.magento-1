<?php
/**
 * Initiate an instance of the "Widthrule" model.
*/
class Duel_Emails_Model_Widthrule extends Mage_Core_Model_Abstract
{

  protected function _construct()
  {
    $this->_init('duel_emails/widthrule');
  }

}