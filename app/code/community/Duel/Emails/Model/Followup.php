<?php
/**
 * Initiate an instance of the "Followup" model.
*/
class Duel_Emails_Model_Followup extends Mage_Core_Model_Abstract
{

  protected function _construct()
  {
    $this->_init('duel_emails/followup');
  }

}