<?php
/**
 * initiates a Collection for the "Widthrule" Model, representing a collection of css rules for the Duel galleries.
*/
class Duel_Emails_Model_Resource_Widthrule_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

  public function _construct()
  {
    $this->_init('duel_emails/widthrule');
  }

}